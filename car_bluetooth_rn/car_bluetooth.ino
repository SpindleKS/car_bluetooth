#include <SoftwareSerial.h>// import the serial library
#include <Servo.h>

SoftwareSerial Genotronex(0, 1); // RX, TX
int ledpin=13; // led on D13 will show blink on / off
int BluetoothData = -1; // the data given from Computer
int centerAngle = 98, servoCurrentAngle = centerAngle,  minAngle = 28, maxAngle = 168, forwardCnt = 0, fwdDefault = 255;
Servo myServo;


void setup() {
  // put your setup code here, to run once:
  Genotronex.begin(9600);
//  Genotronex.println("Bluetooth On please press 1 or 0 blink LED ..");
  pinMode(ledpin,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(10,OUTPUT);
  myServo.write(centerAngle);
  //Serial.begin(9600);
  myServo.attach(9);
}
void loop() {
  
  // put your main code here, to run repeatedly:
   if (Genotronex.available())
   {
     BluetoothData=Genotronex.read();
     Genotronex.println(BluetoothData);
   
//     if(BluetoothData== '1'){   // if number 1 pressed ....
//      digitalWrite(ledpin,1);
//      Genotronex.println("LED  On D13 ON ! ");
//     }
     
     if (BluetoothData == '0')
     {
//      digitalWrite(ledpin,0);
      analogWrite(11, 0);
      analogWrite(5, 0);
      analogWrite(6, 0);
      forwardCnt = 0;
     }

    if(BluetoothData == '1')
    {
      servoCurrentAngle = centerAngle;
      myServo.write(servoCurrentAngle);
    }
     
     if(BluetoothData == 'a') //turn left
     {
      if(servoCurrentAngle < maxAngle)
      {
        servoCurrentAngle += 30;
        myServo.write(servoCurrentAngle);
        
      }
     }
     if (BluetoothData == 'd') //turn right
     {
      if(servoCurrentAngle > minAngle)
      {
        servoCurrentAngle += -30;
        myServo.write(servoCurrentAngle);
        
      }
     }
     if (BluetoothData == 'w')
     {
      
      Genotronex.println("uz prieksu");
      analogWrite(11, 255);
      analogWrite(10, 255);
      
      if (forwardCnt == 0)
      {
        analogWrite(5, 255);
        forwardCnt++;
      }
      else 
      {
        if (forwardCnt <= 3)
          {
            analogWrite(5, fwdDefault - (50 * forwardCnt));


            
          }
      }     
      
      analogWrite(6, 0);
     }
     if (BluetoothData == 's')
     {
      Genotronex.println("atpakal");
      analogWrite(11, 255);  
      analogWrite(10, 255);    
      analogWrite(5, 0);
      analogWrite(6, 255);
      forwardCnt == 0;
      //digitalWrite(4, HIGH);
      //digitalWrite(7, LOW);
     }
   }
   BluetoothData = -1;
  delay(100);// prepare for next data ...
}
