#include <Servo.h>

Servo myServo;
bool left  = false, right = false, accelerate = false, stopper = false;

void setup()
{
  Serial.begin(9600);  
  myServo.attach(9);
  myServo.write(90);
}

void loop()
{  
    int val = 0;    
    if( Serial.available() > 0){
       val = Serial.parseInt();
       Serial.println(val);
    }
    
    if(val == 1){
      left = true;
      right = false;
    }else if(val == 2){
      right = true;
      left = false;
    }else if(val == 3){
      accelerate = true;
    }else if(val == 5){
      stopper = true;
      right = false;
      left = false;
    }

    if(left){
      myServo.write(165);
    }

    if(right){
      myServo.write(35);
    }
    
    if(accelerate){      
      digitalWrite(3, 90);
    }

    if(stopper){
      myServo.write(98); 
      accelerate = false; 
      digitalWrite(3, 0);
      stopper = false;
    }
    
}
